class User {
  String? name;
  String? lastname;
  String? dateBirth;
  String? gender;
  String? imss;
  String? alcaldia;
  String? nationality;
  bool? isModel;
  String? height;
  String? eyesColor;
  String? skinColor;
  String? hairColor;
  String? size;
  List<String>? photosPortafolio;
  String? ine;
  String? voucherHouse;
  String? stateAccount;
  String? rfc;
  String? fm2;

  User(
      {this.name,
      this.lastname,
      this.dateBirth,
      this.gender,
      this.imss,
      this.alcaldia,
      this.nationality,
      this.isModel,
      this.height,
      this.eyesColor,
      this.skinColor,
      this.hairColor,
      this.size,
      this.photosPortafolio,
      this.ine,
      this.voucherHouse,
      this.stateAccount,
      this.rfc,
      this.fm2});

  User.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    lastname = json['lastname'];
    dateBirth = json['dateBirth'];
    gender = json['gender'];
    imss = json['imss'];
    alcaldia = json['alcaldia'];
    nationality = json['nationality'];
    isModel = json['isModel'];
    height = json['height'];
    eyesColor = json['eyesColor'];
    skinColor = json['skinColor'];
    hairColor = json['hairColor'];
    size = json['size'];
    photosPortafolio = json['photosPortafolio'].cast<String>();
    ine = json['ine'];
    voucherHouse = json['voucherHouse'];
    stateAccount = json['stateAccount'];
    rfc = json['rfc'];
    fm2 = json['fm2'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['lastname'] = this.lastname;
    data['dateBirth'] = this.dateBirth;
    data['gender'] = this.gender;
    data['imss'] = this.imss;
    data['alcaldia'] = this.alcaldia;
    data['nationality'] = this.nationality;
    data['isModel'] = this.isModel;
    data['height'] = this.height;
    data['eyesColor'] = this.eyesColor;
    data['skinColor'] = this.skinColor;
    data['hairColor'] = this.hairColor;
    data['size'] = this.size;
    data['photosPortafolio'] = this.photosPortafolio;
    data['ine'] = this.ine;
    data['voucherHouse'] = this.voucherHouse;
    data['stateAccount'] = this.stateAccount;
    data['rfc'] = this.rfc;
    data['fm2'] = this.fm2;
    return data;
  }
}
