import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

class Api {
  final FirebaseFirestore fb = FirebaseFirestore.instance;
  final String path;
  late CollectionReference ref;
  Api(this.path) {
    ref = fb.collection(path);
  }

  Future<DocumentReference> create(Map data) {
    return ref.add(data);
  }
  Future<void> createWithId(String id,Map data) {
    return ref.doc(id).set(data);
  }
}
