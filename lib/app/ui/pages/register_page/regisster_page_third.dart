import 'dart:io';

import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/ui/global_widgets/elevated_button.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/loading_four_stage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class RegisterPageThird extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterController>(
        init: RegisterController(),
        builder: (controller) {
          return Scaffold(
              body: SafeArea(
                  child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: IconButton(
                        onPressed: () {}, icon: Icon(Icons.arrow_back_ios_new)),
                  ),
                  Text("Añade fotos a tu portafolio",
                      style: Theme.of(context).textTheme.headline1),
                  Text("Agrega mínimo 1 foto en cuerpo completo",
                      style: Theme.of(context).textTheme.headline2),
                  // OutlinedButton(
                  //     onPressed: () {
                  //       showmodalBottom(context, controller);
                  //     },
                  //     child: Text("Agregar foto")),
                  SizedBox(
                    height: Get.height / 15,
                  ),
                  Obx(
                    () => Wrap(
                      spacing: 20,
                      runSpacing: 20,
                      children: [
                        Stack(
                          children: [
                            Container(
                                height: Get.height / 4,
                                width: Get.width / 3,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.2),
                                    border: Border.all(color: Colors.green),
                                    borderRadius: BorderRadius.circular(10)),
                                child: controller
                                        .photoPortafolioOne.value.isEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.all(40.0),
                                        child: ElevatedButton(
                                          onPressed: () {
                                            print("joel");
                                            controller.pickerBriefCaseOne();
                                          },
                                          child: FaIcon(
                                            FontAwesomeIcons.plus,
                                            
                                            size: 25,
                                          ),
                                          style: ElevatedButton.styleFrom(
                                              shape: CircleBorder()),
                                        ),
                                      )
                                    : Image.file(File(
                                        controller.photoPortafolioOne.value))),
                            controller.photoPortafolioOne.value.isNotEmpty ?
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: IconButton(
                                  onPressed: () {
                                    controller.photoPortafolioOne.value = "";
                                  },
                                  icon: Icon(
                                    Icons.remove_circle,
                                    size: 55,
                                    color: Colors.blue,
                                  )),
                            ):SizedBox()
                          ],
                        ),
                        Stack(
                          children: [
                            Container(
                                height: Get.height / 4,
                                width: Get.width / 3,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.2),
                                    border: Border.all(color: Colors.green),
                                    borderRadius: BorderRadius.circular(10)),
                                child: controller.photoPortafolioTwo.value.isEmpty
                                        ? Padding(
                                            padding: const EdgeInsets.all(40.0),
                                            child: ElevatedButton(
                                              onPressed: () {
                                                print("joel");
                                                controller.pickerBriefCaseTwo();
                                              },
                                              child: FaIcon(
                                                FontAwesomeIcons.plus,
                                                
                                                size: 25,
                                              ),
                                              style: ElevatedButton.styleFrom(
                                                  shape: CircleBorder()),
                                            ),
                                          )
                                        : Image.file(File(
                                            controller.photoPortafolioTwo.value))
                                    ),
                                   controller.photoPortafolioTwo.value.isNotEmpty ?
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: IconButton(
                                  onPressed: () {
                                    controller.photoPortafolioTwo.value = "";
                                  },
                                  icon: Icon(
                                    Icons.remove_circle,
                                    size: 55,
                                    color: Colors.blue,
                                  )),
                            ):SizedBox() 
                          ],
                        ),
                        Stack(
                          children: [
                            Container(
                                height: Get.height / 4,
                                width: Get.width / 3,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.2),
                                    border: Border.all(color: Colors.green),
                                    borderRadius: BorderRadius.circular(10)),
                                child:  controller.photoPortafolioThree.value.isEmpty
                                        ? Padding(
                                            padding: const EdgeInsets.all(40.0),
                                            child: ElevatedButton(
                                              onPressed: () {
                                                print("joel");
                                                controller.pickerBriefCaseThree();
                                              },
                                              child: FaIcon(
                                                FontAwesomeIcons.plus,
                                                
                                                size: 25,
                                              ),
                                              style: ElevatedButton.styleFrom(
                                                  shape: CircleBorder()),
                                            ),
                                          )
                                        : Image.file(File(
                                            controller.photoPortafolioThree.value))
                                    ),
                                    controller.photoPortafolioThree.value.isNotEmpty ?
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: IconButton(
                                  onPressed: () {
                                    controller.photoPortafolioThree.value = "";
                                  },
                                  icon: Icon(
                                    Icons.remove_circle,
                                    size: 55,
                                    color: Colors.blue,
                                  )),
                            ):SizedBox()
                          ],
                        ),
                        Stack(
                          children: [
                            Container(
                                height: Get.height / 4,
                                width: Get.width / 3,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.2),
                                    border: Border.all(color: Colors.green),
                                    borderRadius: BorderRadius.circular(10)),
                                child: controller.photoPortafolioFour.value.isEmpty
                                        ? Padding(
                                            padding: const EdgeInsets.all(40.0),
                                            child: ElevatedButton(
                                              onPressed: () {
                                                print("joel");
                                                controller.pickerBriefCaseFour();
                                              },
                                              child: FaIcon(
                                                FontAwesomeIcons.plus,
                                                
                                                size: 25,
                                              ),
                                              style: ElevatedButton.styleFrom(
                                                  shape: CircleBorder()),
                                            ),
                                          )
                                        : Image.file(File(
                                            controller.photoPortafolioFour.value))
                                    ),
                                    controller.photoPortafolioFour.value.isNotEmpty ?
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: IconButton(
                                  onPressed: () {
                                    controller.photoPortafolioFour.value = "";
                                  },
                                  icon: Icon(
                                    Icons.remove_circle,
                                    size: 55,
                                    color: Colors.blue,
                                  )),
                            ):SizedBox()
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: Get.height / 7),
                  WElevatedButton(
                      onpressed: () {
                        Get.to(LoadingFourPage(),
                            transition: Transition.rightToLeft);
                      },
                      text: Text("Continuar"),
                      elevatedStyle: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(14),
                          primary: Colors.black,
                          minimumSize: Size(Get.width, 20))),
                  SizedBox(height: Get.height / 30),
                ],
              ),
            ),
          )));
        });
  }

  Future showmodalBottom(context, controller) {
    return showMaterialModalBottomSheet(
      context: context,
      builder: (context) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Text("Camara"),
            onTap: () {
              controller.pickerBriefCaseOne();
            },
          ),
          ListTile(
            title: Text("Galeria"),
            onTap: () {
              controller.imagepickerGallery();
            },
          ),
        ],
      ),
    );
  }
}
