import 'package:appcasting/app/controllers/loading_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class LoadingFirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoadingController>(
      init: LoadingController(),
      builder: (_) {
        return Scaffold(
      body: Center(
      child: Column(
        
        mainAxisAlignment: MainAxisAlignment.center,
       
        children: [
          
          Text("Primera etapa",style: Theme.of(context).textTheme.headline1,),
          Text("Vamos a iniciar con tus datos personales",style: Theme.of(context).textTheme.headline2,),
          SizedBox(height: Get.height / 20,),
          Lottie.asset("assets/lottie/write.json",width: 170,)],
      ),
    ),
    );
      });
  }
}
