import 'dart:io';

import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/ui/global_widgets/elevated_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WIne extends GetView<RegisterController> {
  @override
  Widget build(BuildContext context) {
    
    return SafeArea(
        child: Container(
      height: Get.height,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: IconButton(
                onPressed: () {
                  controller.pageController.previousPage(
                      duration: Duration(seconds: 1), curve: Curves.easeInOut);
                },
                icon: Icon(Icons.arrow_back_ios_new)),
          ),
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "Añadir INE",
                  style: Theme.of(context).textTheme.headline1,
                ),
                Text(
                  "Agregar la foto de tu INE",
                  style: Theme.of(context).textTheme.headline2,
                ),
                SizedBox(
                  height: Get.height / 7,
                ),
                Obx(
                  () => Container(
                      height: Get.height / 3,
                      width: Get.width / 2,
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.5),
                          border: Border.all(color: Colors.green),
                          borderRadius: BorderRadius.circular(10)),
                      child: controller.photoIne.value.isEmpty
                          ? Padding(
                              padding: const EdgeInsets.all(70.0),
                              child: ElevatedButton(
                                onPressed: () {
                                  controller.pickerIne();
                                },
                                child: FaIcon(FontAwesomeIcons.plus),
                                style: ElevatedButton.styleFrom(
                                    shape: CircleBorder()),
                              ),
                            )
                          : Image.file(
                              File(controller.photoIne.value),
                              width: Get.width,
                              height: Get.height,
                            )),
                ),
                SizedBox(
                  height: Get.height / 4,
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: WElevatedButton(
                onpressed: () {
                  controller.pageController.nextPage(
                      duration: Duration(seconds: 1), curve: Curves.easeInOut);
                },
                text: Text("Continuar"),
                elevatedStyle: ElevatedButton.styleFrom(
                    padding: EdgeInsets.all(14),
                    primary: Colors.black,
                    minimumSize: Size(200, 20))),
          ),
        ],
      ),
    ));
  }
}
