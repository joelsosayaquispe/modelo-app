import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/ui/global_widgets/elevated_button.dart';
import 'package:appcasting/app/ui/global_widgets/templateRegister.dart';
import 'package:appcasting/app/ui/global_widgets/textFormField.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_seguro_social.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class WGender extends GetView<RegisterController> {
  const WGender({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TemplateRegister(
      onpressed: () {
        controller.pageControllerOne
            .previousPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        
      },
      title: "Soy",
      subtitle: "",
      button: SizedBox(),
      lottiefile: Image.asset(
        "assets/icons/gender.png",
        width: 100,
      ),
      column: Column(
        children: [
          SizedBox(
            height: Get.height / 8,
          ),
          WElevatedButton(
              onpressed: () {
                controller.controllerGender.value.text = "Mujer";
                controller.pageControllerOne.nextPage(
                    duration: Duration(seconds: 1), curve: Curves.easeInOut);
              },
              text: Text("Mujer"),
              elevatedStyle: ElevatedButton.styleFrom(
                  minimumSize: Size(140, 50), primary: Colors.black)),
          SizedBox(
            height: Get.height / 20,
          ),
          WElevatedButton(
            
            onpressed: () {
              controller.controllerGender.value.text = "Hombre";
              controller.pageControllerOne.nextPage(
                  duration: Duration(seconds: 1), curve: Curves.easeInOut);
            },
            text: Text("Hombre"),
            elevatedStyle: ElevatedButton.styleFrom(
                minimumSize: Size(140, 50), primary: Colors.black),
          )
        ],
      ),
    );
  }
}
