import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/ui/global_widgets/elevated_button.dart';
import 'package:appcasting/app/ui/global_widgets/templateRegister.dart';
import 'package:appcasting/app/ui/global_widgets/textFormField.dart';
import 'package:appcasting/app/ui/pages/login_page/login_page.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:lottie/lottie.dart';

class WUsuario extends GetView<RegisterController> {
  const WUsuario({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var nameController = TextEditingController();
    var lastNameController = TextEditingController();
    return TemplateRegister(
      onpressed: () {
        controller.pageControllerOne
            .previousPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        
      },
      title: "Mi nombre es",
      subtitle: "Así aparecerá en la aplicación y no podras cambiarlo",
      lottiefile: Lottie.asset("assets/lottie/user1.json"),
      column: Column(
        children: [
          InputFormField(
            controller: nameController,
            prefixIcon: Icon(Icons.person),
            labelText: "Nombres",
            validator: (value) {
              value!.isEmpty ? "Nombres de usuario no valido" : null;
            },
          ),
          SizedBox(height: Get.height / 21),
          InputFormField(
            controller: controller.controllerLastname.value,
            prefixIcon: Icon(Icons.person),
            labelText: "Apellidos",
            validator: (value) {
              value!.isEmpty ? "Apellidos de usuario no valido" : null;
            },
          )
        ],
      ),
      button: WElevatedButton(
          onpressed: () {
            controller.pageControllerOne.nextPage(
                duration: Duration(seconds: 1), curve: Curves.easeInOut);
          },
          text: Text("Continuar"),
          elevatedStyle: ElevatedButton.styleFrom(
              padding: EdgeInsets.all(14), primary: Colors.black)),
    );
  }
}
