import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/ui/global_widgets/elevated_button.dart';
import 'package:appcasting/app/ui/global_widgets/templateRegister.dart';
import 'package:appcasting/app/ui/global_widgets/textFormField.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_usuario.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_gender.dart';
import 'package:flutter/material.dart';
import 'package:flutter_holo_date_picker/date_picker.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:sizer/sizer.dart';
import 'package:intl/intl.dart';

class WDate extends GetView<RegisterController> {
  const WDate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return TemplateRegister(
      onpressed: () {
        controller.pageControllerOne
            .previousPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        
      },
      title: "Fecha de nacimiento",
      subtitle: "Tu edad será publica en la aplicación",
      lottiefile: Lottie.asset("assets/lottie/date.json"),
      column: Column(
        children: [
          Obx(
            () => Container(
              
              padding: EdgeInsets.symmetric(horizontal: Get.width / 12,vertical: Get.height / 30),
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.3),
                
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(controller.textDate.value,style: Theme.of(context).textTheme.headline1,),
            )
          ),
          TextButton(
              onPressed: () async {
                controller.datePicker(context);
              },
              child: Text("Seleccionar fecha"))
        ],
      ),
      button: WElevatedButton(
          onpressed: () {
            controller.pageControllerOne.nextPage(
                duration: Duration(seconds: 1), curve: Curves.easeInOut);
          },
          text: Text("Continuar"),
          elevatedStyle: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(14),
            primary: Colors.black
          )),
    );
  }
}
