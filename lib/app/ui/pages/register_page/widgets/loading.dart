import 'package:appcasting/app/routes/app_routes.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_usuario.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoadingPage extends StatefulWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Future.delayed(
    //   Duration(seconds: 3),
    //   ()=>{
    //     Get.toNamed(Routes.REGISTER)
    //   }
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
       
        children: [
          Text("Segunda etapa",style: Theme.of(context).textTheme.headline1,),
          Text("Ahora comenzamos con tus rasgos físicos",style: Theme.of(context).textTheme.headline2,),
          SizedBox(height: Get.height / 20,),
          Image.asset("assets/gifts/loading.gif",width: 170,)],
      ),
    ),
    );
  }
}
