import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/ui/global_widgets/elevated_button.dart';
import 'package:appcasting/app/ui/global_widgets/templateRegister.dart';
import 'package:appcasting/app/ui/global_widgets/textFormField.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_seguro_social.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_nationality.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:direct_select/direct_select.dart';

class WAlcaldia extends GetView<RegisterController> {
  const WAlcaldia({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int selectIndex = 0;
    List<Widget> alcaldias = [
      TextSelect(text: "Alcaldia 1"),
      TextSelect(text: "Alcaldia 2"),
      TextSelect(text: "Alcaldia 3")
    ];
    List<String> alcaldiass = [
      "Alcaldia 1",
      "Alcaldia 2",
      "Alcaldia 3"
    ];
    return TemplateRegister(
      onpressed: () {
        controller.pageControllerOne
            .previousPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        
      },
      title: "Tu alcaldía es:",
      subtitle: "Esto nos ayudara a ubicarte",
      lottiefile: Image.asset(
        "assets/icons/alcaldia.png",
        width: 150,
      ),
      column: Column(
        children: [
          SizedBox(height: Get.height / 21),
          Obx(
            () => DirectSelect(
                mode: DirectSelectMode.tap,
                selectedIndex: controller.selectIndex.value,
                items: alcaldiass.map((e) =>  TextSelect(text: e)).toList(),
                onSelectedItemChanged: (index) {
                  controller.selectIndex.value = index!;
                  controller.controllerAlcaldia.value.text = alcaldiass[index];
                },

                itemExtent: 35.0,
                child:
                    SelectStyle(text: alcaldias[controller.selectIndex.value])),
          ),
          SizedBox(height: Get.height / 15),
        ],
      ),
      button: WElevatedButton(
          onpressed: () {
            controller.pageControllerOne.nextPage(
                duration: Duration(seconds: 1), curve: Curves.easeInOut);
          },
          text: Text("Continuar"),
          elevatedStyle: ElevatedButton.styleFrom(
              padding: EdgeInsets.all(14), primary: Colors.black)),
    );
  }
}

class SelectStyle extends GetView {
  final Widget text;
  const SelectStyle({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 9,
      child: Container(
          width: Get.width,
          height: Get.height / 10,
          child: Align(alignment: Alignment.center, child: text)),
    );
  }
}

class TextSelect extends GetView {
  final String text;
  const TextSelect({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
    );
  }
}
