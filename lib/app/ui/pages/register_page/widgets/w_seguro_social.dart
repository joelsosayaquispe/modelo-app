import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/ui/global_widgets/elevated_button.dart';
import 'package:appcasting/app/ui/global_widgets/templateRegister.dart';
import 'package:appcasting/app/ui/global_widgets/textFormField.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_alcaldia.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_gender.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:sizer/sizer.dart';

class WSeguroSocial extends GetView<RegisterController> {
  const WSeguroSocial({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var nameController = TextEditingController();
    var lastNameController = TextEditingController();
    return TemplateRegister(
      onpressed: () {
        controller.pageControllerOne
            .previousPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        
      },
      title: "Instituto mexicano del seguro social",
      subtitle: "Ingresas tu número de seguridad social (IMSS)",
      lottiefile: Image.asset("assets/icons/hospital.png",width: 150,),
      column: Column(
        children: [
          InputFormField(
            controller: controller.controllerName,
            prefixIcon: Icon(Icons.local_hospital),
            labelText: "IMSS",
            validator: (value) {
              value!.isEmpty ? "Nombres de usuario no valido" : null;
            },
          ),
          SizedBox(height: Get.height / 21),
          
        ],
      ),
      button: WElevatedButton(
          onpressed: () {
            controller.pageControllerOne.nextPage(
                duration: Duration(seconds: 1), curve: Curves.easeInOut);
          },
          text: Text("Continuar"),
          elevatedStyle: ElevatedButton.styleFrom(
            padding: EdgeInsets.all(14),
            primary: Colors.black
          )),
    );
  }
}
