import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/ui/global_widgets/elevated_button.dart';
import 'package:appcasting/app/ui/global_widgets/templateRegister.dart';
import 'package:appcasting/app/ui/global_widgets/textFormField.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/loading_second_stage.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_nationality.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_date.dart';
import 'package:direct_select/direct_select.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:sizer/sizer.dart';

class WModelo extends GetView<RegisterController> {
  const WModelo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int selectIndex = 0;
    List<Widget> alcaldias = [
      TextSelect(text: "México"),
      TextSelect(text: "Perú"),
      TextSelect(text: "Colombia")
    ];
    return TemplateRegister(
      onpressed: () {
        controller.pageControllerOne
            .previousPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        
      },
      title: "¿Eres modelo actualmente?",
      subtitle: "",
      button: SizedBox(),
      lottiefile: Image.asset(
        "assets/icons/girl.png",
        width: 170,
      ),
      column: Column(
        children: [
          SizedBox(
            height: Get.height / 8,
          ),
          WElevatedButton(
              onpressed: () {
                Get.to(LoadingSecondPage());
              },
              text: Text("Mujer"),
              elevatedStyle: ElevatedButton.styleFrom(
                  minimumSize: Size(140, 50), primary: Colors.black)),
          SizedBox(
            height: Get.height / 20,
          ),
          WElevatedButton(
            onpressed: () {
               controller.pageControllermain
            .nextPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        Future.delayed(Duration(seconds: 4), () {
          controller.pageControllermain
            .nextPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        });
            },
            text: Text("Hombre"),
            elevatedStyle: ElevatedButton.styleFrom(
                minimumSize: Size(140, 50), primary: Colors.black),
          )
        ],
      ),
    );
  }
}
