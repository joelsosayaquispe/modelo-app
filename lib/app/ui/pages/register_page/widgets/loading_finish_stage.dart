import 'package:appcasting/app/controllers/loading_controller.dart';
import 'package:appcasting/app/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';



class LoadingFinishPage extends StatefulWidget {
  const LoadingFinishPage({Key? key}) : super(key: key);

  @override
  State<LoadingFinishPage> createState() => _LoadingFinishPageState();
}

class _LoadingFinishPageState extends State<LoadingFinishPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      Get.offAllNamed(Routes.INIT);
    });
  }  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
       
        children: [
          
          Text("Bienvenido",style: Theme.of(context).textTheme.headline1,),
          Text("Haz completado tu registro",style: Theme.of(context).textTheme.headline2,),
          SizedBox(height: Get.height / 20,),
          Lottie.asset("assets/lottie/checking.json",width: 170,)],
      ),
    ),
    );
  }
}