import 'package:appcasting/app/controllers/loading_controller.dart';
import 'package:appcasting/app/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';


class LoadingSecondPage extends StatefulWidget {
  const LoadingSecondPage({Key? key}) : super(key: key);

  @override
  State<LoadingSecondPage> createState() => _LoadingSecondPageState();
}

class _LoadingSecondPageState extends State<LoadingSecondPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      Get.offAllNamed(Routes.REGISTERTWO);
    });
  }  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
       
        children: [
          Text("Segunda etapa",style: Theme.of(context).textTheme.headline1,),
          Text("Ahora comenzamos con tus rasgos físicos",style: Theme.of(context).textTheme.headline2,),
          SizedBox(height: Get.height / 20,),
          Lottie.asset("assets/lottie/girl-eyes.json",width: 170,)],
      ),
    ),
    );
  }
}