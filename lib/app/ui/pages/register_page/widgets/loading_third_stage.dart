import 'package:appcasting/app/controllers/loading_controller.dart';
import 'package:appcasting/app/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';


class LoadingThirdPage extends StatefulWidget {
  const LoadingThirdPage({Key? key}) : super(key: key);

  @override
  State<LoadingThirdPage> createState() => _LoadingThirdPageState();
}

class _LoadingThirdPageState extends State<LoadingThirdPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      Get.offAllNamed(Routes.REGISTERTHIRD);
    });
  }  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
       
        children: [
          Text("Hemos terminado la segunda etapa.",style: Theme.of(context).textTheme.headline1,),
          Text("Ahora sube fotos en tiempo completo",style: Theme.of(context).textTheme.headline2,),
          SizedBox(height: Get.height / 20,),
          Lottie.asset("assets/lottie/photo.json",width: 170,)],
      ),
    ),
    );
  }
}