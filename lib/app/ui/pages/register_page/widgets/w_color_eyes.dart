import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/ui/global_widgets/elevated_button.dart';
import 'package:appcasting/app/ui/global_widgets/templateRegister.dart';
import 'package:appcasting/app/ui/global_widgets/textFormField.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_seguro_social.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_nationality.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:sizer/sizer.dart';
import 'package:direct_select/direct_select.dart';

class WColorEyes extends GetView<RegisterController> {
  const WColorEyes({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int selectIndex = 0;
    List<Widget> colors= [TextSelect(text: "Rojo"),TextSelect(text: "Rojo"),TextSelect(text: "Azul")];
    return TemplateRegister(
      onpressed: () {
        controller.pageControllerTwo
            .nextPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
      },
      title: "Mi color de ojos es:",
      subtitle: "Asi aparecerá en la aplicación y no podras cambiarlo",
      lottiefile: Lottie.asset(
        "assets/lottie/eye.json",
        width: 150,
      ),
      column: Column(
        children: [
          SizedBox(height: Get.height / 21),
          Obx(
            () => DirectSelect(
                mode: DirectSelectMode.tap,
                selectedIndex: controller.selectIndex.value,
                items: colors,
                onSelectedItemChanged: (index) {
                  controller.selectIndex.value = index!;
                },
                itemExtent: 35.0,
                child: SelectStyle(
                    text: colors[controller.selectIndex.value])),
          ),
          SizedBox(height: Get.height / 15),
        ],
      ),
      button: WElevatedButton(
          onpressed: () {
            controller.pageController.nextPage(
                duration: Duration(seconds: 1), curve: Curves.easeInOut);
          },
          text: Text("Continuar"),
          elevatedStyle: ElevatedButton.styleFrom(
              padding: EdgeInsets.all(14), primary: Colors.black)),
    );
  }
}

class SelectStyle extends GetView {
  final Widget text;
  const SelectStyle({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 9,
      child: Container(
          width: Get.width,
          height: Get.height / 10,
          child: Align(alignment: Alignment.center, child: text)),
    );
  }
}

class TextSelect extends GetView {
  final String text;
  const TextSelect({Key? key,required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),);
  }
}
