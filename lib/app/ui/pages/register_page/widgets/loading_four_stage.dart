import 'package:appcasting/app/controllers/loading_controller.dart';
import 'package:appcasting/app/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';


class LoadingFourPage extends StatefulWidget {
  const LoadingFourPage({Key? key}) : super(key: key);

  @override
  State<LoadingFourPage> createState() => _LoadingFourPageState();
}

class _LoadingFourPageState extends State<LoadingFourPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      Get.offAllNamed(Routes.REGISTERFOUR);
    });
  }  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
       
        children: [
          Text("Estas a punto de finalizar",style: Theme.of(context).textTheme.headline1,),
          Text("Adjunta o tómales fotos a tus documentos",style: Theme.of(context).textTheme.headline2,),
          SizedBox(height: Get.height / 20,),
          Lottie.asset("assets/lottie/search-document.json",width: 170,)],
      ),
    ),
    );
  }
}