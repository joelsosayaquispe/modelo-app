import 'package:appcasting/app/ui/pages/register_page/widgets/loading.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/loading_first_stage.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_alcaldia.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_color_eyes.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_color_hair.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_date.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_fm2.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_gender.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_height.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_ine.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_modelo.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_nationality.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_rfc.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_seguro_social.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_size.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_skin_tone.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_state_account.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_usuario.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_voucher_house.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../controllers/register_controller.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class RegisterPageFour extends GetView<RegisterController> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              PageView(
                controller: controller.pageController,
                children: [
                  WIne(),
                  WVoucherHouse(),
                  WStateAccount(),
                  WRfc(),
                  
                  WFm2(),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                      child: Center(
                    child: SmoothPageIndicator(
                      controller: controller.pageController,
                      count: 5,
                      effect: ExpandingDotsEffect(
                        dotHeight: 5,
                        dotWidth: 20,
                        dotColor: Colors.blue.withOpacity(0.5),
                        activeDotColor: Colors.blue,
                      ),
                    ),
                  ))
                ],
              ),
            ],
          ),
        ),
      );
   
  }
}
