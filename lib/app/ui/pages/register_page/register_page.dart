import 'package:appcasting/app/ui/pages/register_page/widgets/loading.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/loading_first_stage.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_alcaldia.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_color_eyes.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_color_hair.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_date.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_gender.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_height.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_modelo.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_nationality.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_seguro_social.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_size.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_skin_tone.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/w_usuario.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../controllers/register_controller.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class RegisterPage extends GetView<RegisterController> {
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        body: SafeArea(
            child: Stack(children: [
      PageView(
        controller: controller.pageControllermain,
        children: [
          Stack(
            children: [
              PageView(

                controller: controller.pageControllerOne,
                children: [
                  WUsuario(),
                  WDate(),
                  WGender(),
                  WSeguroSocial(),
                  WAlcaldia(),
                  WNationality(),
                  WModelo(),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Center(
                    child: SmoothPageIndicator(
                      controller: controller.pageControllerOne,
                      count: 7,
                      effect: ExpandingDotsEffect(
                        dotHeight: 5,
                        dotWidth: 20,
                        dotColor: Colors.blue.withOpacity(0.5),
                        activeDotColor: Colors.blue,
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Segunda etapa",
                  style: Theme.of(context).textTheme.headline1,
                ),
                Text(
                  "Ahora comenzamos con tus rasgos físicos",
                  style: Theme.of(context).textTheme.headline2,
                ),
                SizedBox(
                  height: Get.height / 20,
                ),
                Image.asset(
                  "assets/gifts/loading.gif",
                  width: 170,
                )
              ],
            ),
          ),
          Stack(
            children: [
              PageView(
                controller: controller.pageControllerTwo,
                children: [
                  WHeight(),
                  WColorEyes(),
                  WSkinTone(),
                  WColorHair(),
                  WSize(),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                      child: Center(
                    child: SmoothPageIndicator(
                      controller: controller.pageControllerTwo,
                      count: 5,
                      effect: ExpandingDotsEffect(
                        dotHeight: 5,
                        dotWidth: 20,
                        dotColor: Colors.blue.withOpacity(0.5),
                        activeDotColor: Colors.blue,
                      ),
                    ),
                  ))
                ],
              ),
            ],
          ),
        ],
      ),
    ])));
  }
}
