import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import '../../../controllers/events_controller.dart';

class EventsPage extends StatelessWidget {
  final items = [
    "assets/img/modeltwo.jpg",
    "assets/img/modelfour.jpg",
    "assets/img/modelthree.jpg"
  ];
  List<Widget> listview2 = [
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset(
        "assets/img/model.jpg",
        width: 240,fit: BoxFit.cover,alignment: Alignment.center
      ),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset(
        "assets/img/model.jpg",
        width: 240,fit: BoxFit.cover,alignment: Alignment.center,
      ),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset(
        "assets/img/model.jpg",
        width: 240,fit: BoxFit.cover,alignment: Alignment.center,
      ),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset(
        "assets/img/model.jpg",
        width: 240,fit: BoxFit.cover,alignment: Alignment.center,
      ),
    ),
  ];
  List<Widget> listview = [
    SizedBox(width: Get.width / 100),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    int activeIndex = 0;

    return GetBuilder<EventsController>(
        init: EventsController(),
        builder: (controller) {
          return Scaffold(
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: Get.height/50,),
                      Stack(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child: Image.asset("assets/img/model.jpg",width: Get.width,height: Get.height/2,fit: BoxFit.cover,),
                            ),
                          ),
                          Positioned(
                            left: 10,
                            bottom: 10,
                            child: Text("Activaciones",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),))
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Lanzamiento de marca"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 120,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview2[index];
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Congresos"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 120,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview2[index];
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Alfombra roja"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 120,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview2[index];
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Eventos artísticos"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 120,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview2[index];
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Modelos"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 220,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview[index];
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Modelos"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 220,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview[index];
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Modelos"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 220,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview[index];
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Modelos"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 220,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview[index];
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  Widget buildIndicator(activeindex) => AnimatedSmoothIndicator(
        activeIndex: activeindex,
        count: items.length,
        effect: ExpandingDotsEffect(dotHeight: 9, dotWidth: 14),
      );
  Widget containerPhoto(String url, index) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset(
        "${url}",
        fit: BoxFit.cover,
      ),
    );
  }

  Widget containerPhotoSmall(String url, index) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset(
        "${url}",
        fit: BoxFit.contain,
      ),
    );
  }
}
