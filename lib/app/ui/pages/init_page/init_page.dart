import 'package:appcasting/app/ui/pages/events_page/events_page.dart';
import 'package:appcasting/app/ui/pages/home_page/home_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../controllers/init_controller.dart';

class InitPage extends GetView<InitController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: PageView(
          controller: controller.pageController,
          onPageChanged: (index) {
            controller.indexPageView.value = index;
          },
          children: [HomePage(), EventsPage()],
        ),
      ),
      bottomNavigationBar: Obx(()=> SizedBox(
        height: 60,
        child: BottomNavigationBar(
          elevation: 9,
          
          currentIndex: controller.indexPageView.value,
            onTap: (index) {
              controller.indexPageView.value = index;
               controller.pageController.animateToPage(index,
                        duration: Duration(milliseconds: 300),
                        curve: Curves.easeInOut);
            },
            
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home),label: "Home"),
              BottomNavigationBarItem(icon: Icon(Icons.work_outlined),label: "Events"),
            ]),
      ),)
    );
  }
}
