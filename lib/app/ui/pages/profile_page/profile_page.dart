import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import '../../../controllers/profile_controller.dart';

class ProfilePage extends GetView<ProfileController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        elevation: 9,
        backgroundColor: Colors.white,
        foregroundColor: Colors.yellow,
        onPressed: (){

      },child: Icon(Icons.heart_broken_rounded,color:Colors.red,size: 35,),),
        body: Hero(
          tag: "profile",
          child: CustomScrollView(
              slivers: [
          SliverAppBar( 
            actions: [IconButton(onPressed: (){}, icon: Icon(Icons.menu))],
            leading: IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(Icons.arrow_back_ios)),
            automaticallyImplyLeading: false,
            pinned: true,
            expandedHeight: 400,
            flexibleSpace: FlexibleSpaceBar(
              background: Image.asset("assets/img/model.jpg",
                  fit: BoxFit.cover, height: 370, width: Get.width),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.only(left: 10,right: 10,top: 25),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Lorena,",
                                style: TextStyle(
                                    fontSize: 25, fontWeight: FontWeight.bold),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 7),
                                child: Text("27"),
                              )
                            ],
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "text",
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(15)),
                        child: Text("2.5 mil"),
                      )
                    ],
                  ),
                  SizedBox(
                    height: Get.height / 26,
                  ),
                  Column(
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.remove_red_eye_sharp,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "text",
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.remove_red_eye_sharp,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "text",
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.remove_red_eye_sharp,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "text",
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.remove_red_eye_sharp,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "text",
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.handshake,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "text",
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
              ],
            ),
        ));
  }
}
// Hero(
//         tag: "profile",
//         child: SafeArea(
//             child: Column(
//           children: [
//             Image.asset("assets/img/model.jpg",
//                 fit: BoxFit.cover, height: 370, width: Get.width),
//             SizedBox(height: Get.height/22,),
//             Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 10),
//               child: Column(
//                 children: [
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           Row(children: [
//                             Text("Lorena,",style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
//                           Padding(
//                             padding: const EdgeInsets.only(bottom: 7),
//                             child: Text("27"),
//                           )
//                           ],),
//                           Align(
//                             alignment: Alignment.centerLeft,
//                             child: Text(
//                                     "text",
//                                     style: TextStyle(color: Colors.grey),
//                                   ),
//                           ),
//                         ],
//                       ),
//                       Container(
//                         padding: EdgeInsets.all(10),
//                         decoration: BoxDecoration(
//                           color: Colors.grey[200],
//                           borderRadius: BorderRadius.circular(15)
//                         ),
//                         child: Text("2.5 mil"),
//                       )
                      
//                     ],
//                   ),
                  
//                   SizedBox(height: Get.height/26,),
//               Column(
//                 children: [
//                   Row(
//                     children: [
//                       Icon(Icons.remove_red_eye_sharp,color: Colors.grey,),
//                       SizedBox(
//                         width: 10,
//                       ),
//                       Text(
//                         "text",
//                         style: TextStyle(color: Colors.grey),
//                       )
//                     ],
//                   ),
//                   Row(
//                     children: [
//                       Icon(Icons.remove_red_eye_sharp,color: Colors.grey,),
//                       SizedBox(
//                         width: 10,
//                       ),
//                       Text(
//                         "text",
//                         style: TextStyle(color: Colors.grey),
//                       )
//                     ],
//                   ),
//                   Row(
//                     children: [
//                       Icon(Icons.remove_red_eye_sharp,color: Colors.grey,),
//                       SizedBox(
//                         width: 10,
//                       ),
//                       Text(
//                         "text",
//                         style: TextStyle(color: Colors.grey),
//                       )
//                     ],
//                   ),
//                   Row(
//                     children: [
//                       Icon(Icons.remove_red_eye_sharp,color: Colors.grey,),
//                       SizedBox(
//                         width: 10,
//                       ),
//                       Text(
//                         "text",
//                         style: TextStyle(color: Colors.grey),
//                       )
//                     ],
//                   ),
//                   Row(
//                     children: [
//                       Icon(Icons.handshake,color: Colors.grey,),
//                       SizedBox(
//                         width: 10,
//                       ),
//                       Text(
//                         "text",
//                         style: TextStyle(color: Colors.grey),
//                       )
//                     ],
//                   ),
//                 ],
//               )
//                 ],
//               ),
//             ),
            
//           ],
//         )),
//       ),
//       floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//       floatingActionButton: FloatingActionButton(
//         elevation: 9,
//         backgroundColor: Colors.purple,
//         foregroundColor: Colors.yellow,
//         onPressed: (){

//       },child: Icon(Icons.heart_broken_rounded,color:Colors.red,size: 35,),),
