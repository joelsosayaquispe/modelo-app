import 'package:appcasting/app/controllers/auth_controller.dart';
import 'package:appcasting/app/routes/app_routes.dart';
import 'package:appcasting/app/ui/global_widgets/textFormField.dart';
import 'package:appcasting/app/ui/pages/register_page/register_page.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/loading.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/loading_first_stage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../controllers/login_controller.dart';
import 'package:sizer/sizer.dart';

class LoginPage extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(
        init: AuthController(),
        builder: (_) {
          return Scaffold(
            backgroundColor: Theme.of(context).accentColor,
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 35),
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height),
                  child: Center(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/icons/modellogin.png",height: 140,),
                          SizedBox(height: 5.h,),
                          textformfield("Correo", 90.w, _.emailController,
                              TextInputType.emailAddress,Icon(Icons.email),false),
                          SizedBox(
                            height: 3.h,
                          ),
                          textformfield("Contraseña", 90.w, _.passwordController,
                              TextInputType.text,Icon(Icons.key),true),
                          SizedBox(height: 2.h,),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Text("¿Olvidaste tu contraseña?",style: TextStyle(color: Colors.green),)),
                          SizedBox(
                            height: 16.h,
                          ),
                           ElevatedButton(
                              onPressed: () {
                                _.register(_.emailController.text,
                                    _.passwordController.text);
                              },
                              child: Text("INICIAR SESIÓN"),
                              style: ElevatedButton.styleFrom(
                                minimumSize: Size(Get.width/1.2,50 ),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                primary: Colors.green,
                                
                              ),
                            ),
                          
                          SizedBox(
                            height: 2.h,
                          ),
                          SizedBox(
                            width: Get.width/1.3,
                            child:  Text("Al registrarte, aceptas nuestros términos y politicas de privacidad",textAlign: TextAlign.center,)),
                          SizedBox(
                            height: 5.h,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("¿no tiene una cuenta?"),
                              SizedBox(
                                width: 5.w,
                              ),
                              GestureDetector(
                                  onTap: () {
                                    Get.toNamed(Routes.LOADINGFIRST);
                                  },
                                  child: Text(
                                    "Registrarse",
                                    style: TextStyle(fontWeight: FontWeight.bold,color: Colors.green),
                                  ))
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }
}
