import 'package:appcasting/app/routes/app_routes.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import '../../../controllers/home_controller.dart';

class HomePage extends StatelessWidget {
  int activeIndex = 0;
  final items = [
    "assets/img/modeltwo.jpg",
    "assets/img/modelfour.jpg",
    "assets/img/modelthree.jpg"
  ];
  List<Widget> listview = [
    SizedBox(width: Get.width / 100),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Image.asset("assets/img/model.jpg"),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        init: HomeController(),
        builder: (controller) {
          return Scaffold(
            body: SafeArea(
              child: Container(
                  child: Obx(
                () => SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                                onPressed: () {}, icon: Icon(Icons.menu)),
                            GestureDetector(
                              onTap: () {
                                Get.toNamed(Routes.PROFILE);
                              },
                              child: Hero(
                                tag: "profile",
                                child: CircleAvatar(
                                  child: ClipRRect(
                                      child:
                                          Image.asset("assets/icons/girl.png")),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    'Hi Lorena',
                                    style:
                                        Theme.of(context).textTheme.headline1,
                                    textAlign: TextAlign.left,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  FaIcon(FontAwesomeIcons.hand,
                                      color: Colors.yellow),
                                ],
                              ),
                              Text("Welcome to Home",
                                  style: Theme.of(context).textTheme.headline2),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text("The highlights for you",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold))),
                          ),
                          SizedBox(
                            height: Get.height / 45,
                          ),
                          Stack(
                            children: [
                              CarouselSlider.builder(
                                  options: CarouselOptions(
                                    height: 300,
                                    aspectRatio: 16 / 9,
                                    viewportFraction: 0.7,
                                    initialPage: 0,
                                    onPageChanged: (index, reason) {
                                      controller.activeIndex.value = index;
                                    },
                                    enableInfiniteScroll: true,
                                    reverse: false,
                                    autoPlay: true,
                                    autoPlayInterval: Duration(seconds: 3),
                                    autoPlayAnimationDuration:
                                        Duration(milliseconds: 800),
                                    autoPlayCurve: Curves.fastOutSlowIn,
                                    enlargeCenterPage: true,
                                    scrollDirection: Axis.horizontal,
                                  ),
                                  itemCount: items.length,
                                  itemBuilder: (context, index, realIndex) {
                                    final urlImage = items[index];
                                    return containerPhoto(urlImage, index);
                                  }),
                            ],
                          ),
                          SizedBox(
                            height: Get.height / 35,
                          ),
                          buildIndicator(controller.activeIndex.value)
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Modelos"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 220,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview[index];
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Edecanes"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 220,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview[index];
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.height / 25,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Actores"),
                                Text(
                                  "See all",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 220,
                            child: ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: listview.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(
                                  width: Get.width / 20,
                                );
                              },
                              itemBuilder: (context, index) {
                                return listview[index];
                              },
                            ),
                          ),
                          SizedBox(
                            height: Get.height / 25,
                          ),
                          Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Extras"),
                                    Text(
                                      "See all",
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                height: 220,
                                child: ListView.separated(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: listview.length,
                                  separatorBuilder: (context, index) {
                                    return SizedBox(
                                      width: Get.width / 20,
                                    );
                                  },
                                  itemBuilder: (context, index) {
                                    return listview[index];
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
              )),
            ),
          );
        });
  }

  Widget containerPhoto(String url, index) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: Image.asset(
            "${url}",
            width: Get.width,
            height: Get.height,
            fit: BoxFit.fill,
          ),
        ),
        Positioned(
          left: 10,
          bottom: 10,
          child: Container(
            width: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(6),
                  decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(15)),
                  child: Text(
                    "21 age",
                    style: TextStyle(color: Colors.white,),
                  ),
                ),
                Text(
                  "Modelo nacida en méxico con inteligencia mayor al promedio",
                  style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                ),
                Text(
                  "Read...",
                  style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget buildIndicator(activeindex) => AnimatedSmoothIndicator(
        activeIndex: activeindex,
        count: items.length,
        effect: ExpandingDotsEffect(dotHeight: 9, dotWidth: 14),
      );
}
