import 'package:appcasting/app/controllers/introduction_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class IntroductionthreePage extends GetView<IntroductionController> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        height: Get.height,
        width: Get.width,
        child: Container(
          width: 50,
          margin: EdgeInsets.only(top:400,left: 100,right: 100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Lorem ipsum dolor sit amet",style: TextStyle(color: Colors.white,fontSize: 27,fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
        
              Text("Donec scelerisque efficitur lorem at aliquam. Donec vel sem neque",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),textAlign: TextAlign.center),
             
            ],
          ),
        ),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/img/introduction/model3.jpg"),
                fit: BoxFit.cover)),
      ),
    );
  }
}