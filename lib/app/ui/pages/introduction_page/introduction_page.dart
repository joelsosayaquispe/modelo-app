import 'package:appcasting/app/routes/app_routes.dart';
import 'package:appcasting/app/ui/pages/introduction_page/widgets/introductionone.dart';
import 'package:appcasting/app/ui/pages/introduction_page/widgets/introductionthree.dart';
import 'package:appcasting/app/ui/pages/introduction_page/widgets/introductiontwo.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../controllers/introduction_controller.dart';

class IntroductionPage extends GetView<IntroductionController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Stack(children: [
        PageView(
          onPageChanged: (index) {
            controller.indexPage.value = index;
          },
          controller: controller.pageController,
          children: [
            IntroductiononePage(),
            IntroductiontwoPage(),
            IntroductionthreePage()
          ],
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Obx(
              () => Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SmoothPageIndicator(
                      effect: ExpandingDotsEffect(
                        dotHeight: 9,
                        dotWidth: 20,
                        dotColor: Colors.grey,
                        activeDotColor: Colors.white,
                      ),
                      controller: controller.pageController,
                      count: 3),
                  SizedBox(height: 10),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.blue,
                          minimumSize: Size(Get.width / 1.5, 35)),
                      onPressed: () {
                        controller.indexPage != 2
                            ? controller.pageController.nextPage(
                                duration: Duration(seconds: 1),
                                curve: Curves.easeInOut)
                            : Get.offAndToNamed(Routes.LOGIN);
                      },
                      child: Text(
                        controller.indexPage == 2 ? "Get Stated" : "Next",
                        style: TextStyle(color: Colors.white),
                      )),
                ],
              ),
            ))
      ]),
    ));
  }
}
