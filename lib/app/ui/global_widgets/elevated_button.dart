import 'package:get/get.dart';

import 'package:flutter/material.dart';

class WElevatedButton extends GetView {
  final onpressed;
  final text;
  final elevatedStyle;
  const WElevatedButton({Key? key,this.onpressed,this.text,this.elevatedStyle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(onPressed: onpressed, child: text,style: elevatedStyle,);
  }
}
