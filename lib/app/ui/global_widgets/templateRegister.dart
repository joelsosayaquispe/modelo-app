import 'package:appcasting/app/controllers/register_controller.dart';
import 'package:appcasting/app/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class TemplateRegister extends GetView<RegisterController> {
  final String? title;
  final String? subtitle;
  final lottiefile;
  final column;
  final Widget? button;
  final void Function()? onpressed;
  const TemplateRegister(
      {Key? key,
      this.onpressed,
      this.title,
      this.subtitle,
      this.lottiefile,
      this.column,
      this.button})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: IconButton(
                      onPressed: onpressed, icon: Icon(Icons.arrow_back_ios_new)),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title!,
                        style: Theme.of(context).textTheme.headline1,
                      ),
                      Text(subtitle!,
                          style: Theme.of(context).textTheme.headline2)
                    ],
                  ),
                ),
                SizedBox(height: Get.height / 12),
                lottiefile,
                SizedBox(height: Get.height / 10),
                column,
                SizedBox(height: Get.height / 10),
                button!
              ],
            ),
          ),
        ),
      ),
    );
  }
}
