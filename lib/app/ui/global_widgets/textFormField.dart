import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget textformfield(String labeltext, double w, 
TextEditingController con,
TextInputType inputType,
Icon prefixicon,
bool isPassword) {
  return SizedBox(
    width: w,
    child: TextFormField(
      controller: con,
      keyboardType: inputType,
       obscureText: isPassword,
      validator: (value) {
       return  value!.isEmpty ? "Debe llenar el campo" : null ;
      },
      decoration: InputDecoration(
        labelText: labeltext,
        prefixIcon:prefixicon 
      ),
      onSaved: (value) {
        con.text = value!;
      },
    ),
  );
}

Widget textformfieldPassword(String labeltext, double w, TextEditingController con,TextInputType inputType) {
  return SizedBox(
    width: w,
    child: TextFormField(
      controller: con,
      obscureText: true,
      keyboardType: inputType,
      validator: (value) {
       return  value!.isEmpty ? "Debe llenar el campo" : null ;
      },
      decoration: InputDecoration(
        labelText: labeltext,
        border: OutlineInputBorder(),
      ),
      onSaved: (value) {
        con.text = value!;
      },
    ),
  );
}

class InputFormField extends GetView {
  final Widget? prefixIcon;
  final String? labelText;
  final String? Function(String?)? validator;
final TextEditingController controller;
  const InputFormField({Key? key,this.prefixIcon,this.labelText,this.validator,required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
    validator: validator,
    decoration: InputDecoration(
      prefixIcon: prefixIcon,
      fillColor: Colors.black,
      labelStyle: TextStyle(color: Colors.black),

      focusedBorder:
          OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
      labelText: labelText,
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
    ),
  );
  }
}
