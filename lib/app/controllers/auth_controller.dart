import 'package:appcasting/app/routes/app_routes.dart';
import 'package:appcasting/app/ui/pages/login_page/login_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

class AuthController extends GetxController {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  FirebaseAuth _auth = FirebaseAuth.instance;
  late Rx<User?> firebaseUser;
  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    firebaseUser = Rx<User?>(_auth.currentUser);
    firebaseUser.bindStream(_auth.userChanges());
    ever(firebaseUser, setInitialScreen);
  }

  setInitialScreen(User? user) {
    if (user != null) {
      Get.offAllNamed(Routes.HOME);
    } else {
      Get.offAllNamed(Routes.LOGIN);
    }
  }

  void register(String email, String password) {
    _auth
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((value) => Get.offAllNamed(Routes.LOGIN))
        .catchError((val) {
      Get.snackbar("Registro no completaod", "Vuelva a intentarlo");
    });
  }

  void login(String email, String password) {
    _auth
        .signInWithEmailAndPassword(email: email, password: password)
        .then((value) => Get.offAllNamed(Routes.HOME))
        .catchError((val) {
      Get.snackbar("Usuario invalido", "Vuelva a intentarlo");
    });
  }

  void signOut() {
    _auth.signOut();
  }
}
