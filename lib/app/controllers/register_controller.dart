import 'dart:io';

import 'package:appcasting/app/data/models/User.dart';
import 'package:appcasting/app/data/services/api.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_holo_date_picker/date_picker.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class RegisterController extends GetxController {
  RxBool isCreate = false.obs;
  FirebaseStorage fs = FirebaseStorage.instance;
  Api api = new Api("users");
  XFile? file;
  RxInt numberPage = 0.obs;
  RxString textDate = "".obs;
  RxInt initialpage = 0.obs;
  late PageController pageController = PageController(initialPage: 0);

  late PageController pageControllerOne;
  PageController pageControllerTwo = PageController(initialPage: 0);
  PageController pageControllermain = PageController(initialPage: 0);
  TextEditingController controllerName = TextEditingController();
  Rx<TextEditingController> controllerLastname = TextEditingController().obs;
  Rx<TextEditingController> controllerDate = TextEditingController().obs;
  Rx<TextEditingController> controllerGender = TextEditingController().obs;
  Rx<TextEditingController> controllerImss = TextEditingController().obs;
  Rx<TextEditingController> controllerAlcaldia = TextEditingController().obs;
  Rx<TextEditingController> controllerNationality = TextEditingController().obs;
  RxBool controllerModel = false.obs;
  Rx<TextEditingController> controllerHeight = TextEditingController().obs;
  Rx<TextEditingController> controllerEyes = TextEditingController().obs;
  Rx<TextEditingController> controllerSkin = TextEditingController().obs;
  Rx<TextEditingController> controllerHair = TextEditingController().obs;
  Rx<TextEditingController> controllerSize = TextEditingController().obs;
  @override
  void onInit() {
    // TODO: implement onInit
    print("object");
    super.onInit();
    pageControllerOne = PageController(initialPage: initialpage.value);
  }

  

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }
void changepageOne() {
    initialpage.value = 6;
    update();
  }
  void datePicker(context) async {
    var datapicked = await DatePicker.showSimpleDatePicker(context,
        initialDate: DateTime(1980),
        lastDate: DateTime(2022),
        firstDate: DateTime(2000),
        dateFormat: "dd/MM/yyyy",
        looping: true,
        confirmText: "Aceptar",
        titleText: "Seleccionar fecha de nacimiento",
        cancelText: "Cancelar");
    DateFormat formatter = DateFormat('yyyy-MM-dd');
    var datestring = formatter.format(datapicked!);

    textDate.value = datestring;
    update();

    print(datestring);
  }

  //alcaldia
  RxInt selectIndex = 0.obs;
  List<Widget> alcaldias = [
    Text("Alcaldia 1"),
    Text("Alcaldia 2"),
    Text("Alcaldia 3")
  ];

  //fotos de portafolio
  int countPhotos = 0;
  RxString photoPortafolioOne = "".obs;
  RxString photoPortafolioTwo = "".obs;
  RxString photoPortafolioThree = "".obs;
  RxString photoPortafolioFour = "".obs;

  RxString photoIne = "".obs;
  RxString photoVoucherHouse = "".obs;
  RxString photoStateAccount = "".obs;
  RxString photoRfc = "".obs;
  RxString photoFm2 = "".obs;
  Future pickerBriefCaseOne() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.gallery);
    photoPortafolioOne.value = file!.path;
  }

  Future pickerBriefCaseTwo() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.gallery);
    photoPortafolioTwo.value = file!.path;
  }

  Future pickerBriefCaseThree() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.gallery);
    photoPortafolioThree.value = file!.path;
  }

  Future pickerBriefCaseFour() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.gallery);
    photoPortafolioFour.value = file!.path;
  }

  Future pickerIne() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.gallery);
    photoIne.value = file!.path;
  }

  Future pickerVoucherHouse() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.camera);
    photoVoucherHouse.value = file!.path;
  }

  Future pickerStateAccount() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.camera);
    photoStateAccount.value = file!.path;
  }

  Future pickerRfc() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.camera);
    photoRfc.value = file!.path;
  }

  Future pickerFm2() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.camera);
    photoFm2.value = file!.path;
  }

  Future imagepickerGallery() async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.camera);

    switch (countPhotos) {
      case 0:
        photoPortafolioOne.value = file!.path;
        break;
      case 1:
        photoPortafolioTwo.value = file!.path;
        break;
      case 2:
        photoPortafolioThree.value = file!.path;
        break;
      case 3:
        photoPortafolioFour.value = file!.path;
        break;
    }
    countPhotos++;
    if (file != null) {
      update();
      print("photaaaao saaaassubaiaaadasdds");
    }
    print("jaaaoelassaaaaaaassddddd");
    print(countPhotos);
  }

  Future<void> imagepickerCamera() async {
    file = await ImagePicker().pickImage(source: ImageSource.camera);
    switch (countPhotos) {
      case 0:
        photoPortafolioOne.value = file!.path;
        break;
      case 1:
        photoPortafolioTwo.value = file!.path;
        break;
      case 2:
        photoPortafolioThree.value = file!.path;
        break;
      case 3:
        photoPortafolioFour.value = file!.path;
        break;
    }
    countPhotos++;
    if (file != null) {
      update();
    }
    print("wwwjoelasdddddssdfffffdddd");
    print(countPhotos);
  }

  uploadPhotoOne() async {
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoPortafolioOne.value));
    var url = await ref.getDownloadURL();
    photoPortafolioOne.value = url;
  }

  uploadPhotoTwo() async {
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoPortafolioTwo.value));
    var url = await ref.getDownloadURL();
    photoPortafolioTwo.value = url;
  }

  uploadPhotoThree() async {
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoPortafolioThree.value));
    var url = await ref.getDownloadURL();
    photoPortafolioThree.value = url;
  }

  uploadPhotoFour() async {
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoPortafolioFour.value));
    var url = await ref.getDownloadURL();
    photoPortafolioFour.value = url;
  }

  uploadPhotoIne() async {
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoIne.value));
    var url = await ref.getDownloadURL();
    photoIne.value = url;
  }

  uploadPhotoVoucher() async {
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoVoucherHouse.value));
    var url = await ref.getDownloadURL();
    photoVoucherHouse.value = url;
  }

  uploadPhotoStateAccount() async {
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoStateAccount.value));
    var url = await ref.getDownloadURL();
    photoStateAccount.value = url;
  }

  uploadPhotoRfc() async {
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoRfc.value));
    var url = await ref.getDownloadURL();
    photoRfc.value = url;
  }

  uploadPhotoFm2() async {
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoFm2.value));
    var url = await ref.getDownloadURL();
    photoFm2.value = url;
  }

  Future<void> uploads() async {
    uploadPhotoOne();
    uploadPhotoTwo();
    uploadPhotoThree();
    uploadPhotoFour();
    uploadPhotoFm2();
    uploadPhotoIne();
    uploadPhotoRfc();
    uploadPhotoStateAccount();
    uploadPhotoVoucher();
  }

  createUser() async {
    FirebaseStorage fs = FirebaseStorage.instance;
    await uploads();
    final ref = fs.ref("users/").child(DateTime.now().toString());
    ref.putFile(File(photoPortafolioOne.value));
    User user = new User(
        name: controllerName.value.text,
        lastname: controllerLastname.value.text,
        dateBirth: controllerDate.value.text,
        gender: controllerGender.value.text,
        imss: controllerImss.value.text,
        alcaldia: controllerAlcaldia.value.text,
        nationality: controllerNationality.value.text,
        isModel: controllerModel.value,
        height: controllerHeight.value.text,
        eyesColor: controllerEyes.value.text,
        skinColor: controllerSkin.value.text,
        hairColor: controllerHair.value.text,
        size: controllerSize.value.text,
        photosPortafolio: [
          photoPortafolioOne.value,
          photoPortafolioTwo.value,
          photoPortafolioThree.value,
          photoPortafolioFour.value
        ],
        ine: photoIne.value,
        fm2: photoFm2.value,
        rfc: photoRfc.value,
        stateAccount: photoStateAccount.value,
        voucherHouse: photoVoucherHouse.value);
    api.create(user.toJson()).then((value) => isCreate.value = true);
  }
}
