abstract class Routes {
  static const INITIAL = '/';
  static const INTRODUCTION = '/introduction';
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const REGISTERTWO = '/registertwo';
  static const REGISTERTHIRD = '/registerthird';
   static const REGISTERFOUR = '/registerfour';
  static const LOADINGFIRST = '/loadingfirst';
  static const LOADINGSECOND = '/loadingsecond';
  static const PROFILE = "/profile";
  static const INIT = '/init';
  static const EVENTS = "/events";
}
