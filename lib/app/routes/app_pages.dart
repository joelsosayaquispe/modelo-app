import 'package:appcasting/app/bindings/events_binding.dart';
import 'package:appcasting/app/bindings/home_binding.dart';
import 'package:appcasting/app/bindings/init_binding.dart';
import 'package:appcasting/app/bindings/introduction_binding.dart';
import 'package:appcasting/app/bindings/loading_binding.dart';
import 'package:appcasting/app/bindings/login_binding.dart';
import 'package:appcasting/app/bindings/profile_binding.dart';
import 'package:appcasting/app/bindings/register_binding.dart';
import 'package:appcasting/app/routes/app_routes.dart';
import 'package:appcasting/app/ui/pages/events_page/events_page.dart';
import 'package:appcasting/app/ui/pages/home_page/home_page.dart';
import 'package:appcasting/app/ui/pages/init_page/init_page.dart';
import 'package:appcasting/app/ui/pages/introduction_page/introduction_page.dart';
import 'package:appcasting/app/ui/pages/login_page/login_page.dart';
import 'package:appcasting/app/ui/pages/profile_page/profile_page.dart';
import 'package:appcasting/app/ui/pages/register_page/regisster_page_third.dart';
import 'package:appcasting/app/ui/pages/register_page/register_page.dart';
import 'package:appcasting/app/ui/pages/register_page/register_page_four.dart';
import 'package:appcasting/app/ui/pages/register_page/regiter_page_two.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/loading_first_stage.dart';
import 'package:appcasting/app/ui/pages/register_page/widgets/loading_second_stage.dart';
import 'package:get/get.dart';


abstract class AppPages {

  static final pages = [
    GetPage(name: Routes.LOGIN, page:()=> LoginPage(),binding: LoginBinding(),transition: Transition.zoom),
    GetPage(name: Routes.REGISTER, page:()=> RegisterPage(), binding:RegisterBinding() ,transition: Transition.zoom),
        GetPage(name: Routes.REGISTERTWO, page:()=> RegisterPageTwo(), binding:RegisterBinding() ,transition: Transition.zoom),
     GetPage(name: Routes.REGISTERTHIRD, page:()=> RegisterPageThird(), binding:RegisterBinding() ,transition: Transition.zoom),
     GetPage(name: Routes.REGISTERFOUR, page:()=> RegisterPageFour(), binding:RegisterBinding() ,transition: Transition.zoom),
    GetPage(name: Routes.INTRODUCTION, page:()=> IntroductionPage(), binding:IntroductionBinding() ),
     GetPage(name: Routes.INIT, page:()=> InitPage(), binding:InitBinding() ,transition: Transition.zoom),
    GetPage(name: Routes.HOME, page:()=> HomePage(), binding:HomeBinding() ,transition: Transition.zoom),
    GetPage(name: Routes.LOADINGFIRST, page:()=> LoadingFirstPage(), binding:LoadingBinding() ,transition: Transition.zoom),
    GetPage(name: Routes.EVENTS, page:()=> EventsPage(), binding:EventsBinding() ,transition: Transition.zoom),
    GetPage(name: Routes.PROFILE, page:()=> ProfilePage(), binding:ProfileBinding() ),
    
  ];
}